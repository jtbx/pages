# pages

My Codeberg Pages site.

This mainly contains HTML-format manuals for my programs and scripts, in /man.

To generate the HTML manual pages, clone the repository and enter the `man` folder. Run the script genpages, but note that you will need mandoc for this. http://mandoc.bsd.lv/

The directory /man/lua contains Roff-format Lua manuals (converted via pandoc). These are downloaded by my script [luaman](https://codeberg.org/jtbx/luaman).